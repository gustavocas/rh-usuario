package com.gustavo.usuario.resources;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gustavo.usuario.entities.Usuario;
import com.gustavo.usuario.repositories.UsuarioRepository;

@RestController
@RequestMapping(value="usuario")
public class UsuarioResouce {
	
	@Autowired
	UsuarioRepository repositoryUsuario;
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<Usuario> listarPorId(@PathVariable Integer id) throws Exception{
		
		Optional<Usuario> obj = repositoryUsuario.findById(id);
		Usuario usu = obj.orElseThrow(() -> new Exception("Codigo invalido: " + id));
		return ResponseEntity.ok(usu);
	}
	
	@GetMapping(value = "/buscar")
	public ResponseEntity<Usuario> listarPorId(@RequestParam String email) throws Exception{
		
		Usuario obj = repositoryUsuario.findByEmail(email);
		return ResponseEntity.ok(obj);
	}

}
