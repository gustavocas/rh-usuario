package com.gustavo.usuario.config;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.gustavo.usuario.entities.Regras;
import com.gustavo.usuario.entities.Usuario;
import com.gustavo.usuario.repositories.RegraRepository;
import com.gustavo.usuario.repositories.UsuarioRepository;

@Configuration
public class ConfiguracaoInicial {
	
	@Autowired
	private UsuarioRepository repositoryUsuario;
	
	@Autowired
	private RegraRepository repositoryRegras;
	
	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder(){
		return new BCryptPasswordEncoder();
	}
	
	@Bean
	public void gerarMassaDados() throws ParseException {
		
		
		
		System.out.println("Inserindo usuario");
		Regras r1 = new Regras(null,"ROLE_OPERATOR");
		Regras r2 = new Regras(null,"ROLE_ADMIN");
		
		Usuario u1 = new Usuario(null,"gugu","gugu",bCryptPasswordEncoder().encode("123"));
		u1.getRegras().add(r1);
		Usuario u2 = new Usuario(null,"admin","admin",bCryptPasswordEncoder().encode("123"));
		u2.getRegras().add(r1);
		u2.getRegras().add(r2);
		
		repositoryRegras.save(r1);
		repositoryRegras.save(r2);
		repositoryUsuario.save(u1);
		repositoryUsuario.save(u2);
		
	}
}

