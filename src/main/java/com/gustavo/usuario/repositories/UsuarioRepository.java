package com.gustavo.usuario.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gustavo.usuario.entities.Usuario;


public interface UsuarioRepository extends JpaRepository<Usuario, Integer>{

	Usuario findByEmail(String email);
	
}