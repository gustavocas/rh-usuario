package com.gustavo.usuario.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gustavo.usuario.entities.Regras;


public interface RegraRepository extends JpaRepository<Regras, Integer>{

}